#!/usr/bin/env python
from setuptools import setup

setup(name='swr2disk',
      version='0.1',
      description='',
      author='Azkali Manad',
      author_email='a.ffcc7@gmail.com',
      url='https://gitlab.com/l4t-community/gnu-linux/swr2disk/',
      install_requires=[
        "ninfs @ git+https://github.com/ihaveamac/ninfs@2.0",
        "patool",
        "filesplit",
        "clint",
        "brotlicffi",
        "sh",
        "requests",
        "PyInquirer",
        "envbash"
      ]
)
