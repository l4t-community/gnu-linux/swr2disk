#!/usr/bin/env python

# Author : n0fate
# E-Mail rapfer@gmail.com, n0fate@live.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
# Using structures defined in Wikipedia("http://en.wikipedia.org/wiki/GUID_Partition_Table")
import getopt
import sys
import struct

import binascii

LBA_SIZE = 512

PRIMARY_GPT_LBA = 1

OFFSET_CRC32_OF_HEADER = 16
GPT_HEADER_FORMAT = "<8sIIIIQQQQ16sQIII420x"
GUID_PARTITION_ENTRY_FORMAT = "<16s16sQQQ72s"

def get_lba(fhandle, entry_number, count):
    fhandle.seek(LBA_SIZE*entry_number)
    fbuf = fhandle.read(LBA_SIZE*count)
    
    return fbuf

def set_lba(fhandle, entry_number, data):
    fhandle.seek(LBA_SIZE*entry_number)
    fhandle.write(data)

def get_gpt_header(fhandle, fbuf, lba):
    fbuf = get_lba(fhandle, lba, 1)
    
    gpt_header = struct.unpack(GPT_HEADER_FORMAT, fbuf[:512])
    
    crc32_header_value = calc_header_crc32(fbuf, gpt_header[2])
    
    return gpt_header, crc32_header_value, fbuf

def set_gpt_header(fhandle, gpt_header_bin, lba):
    set_lba(fhandle, lba, gpt_header_bin)

def make_nop(byte):
    nop_code = 0x00
    pk_nop_code = struct.pack("=B", nop_code)
    nop = pk_nop_code*byte
    return nop

def calc_header_crc32(fbuf, header_size):
    nop = make_nop(4)
    
    clean_header = fbuf[:OFFSET_CRC32_OF_HEADER] + nop + fbuf[OFFSET_CRC32_OF_HEADER+4:header_size]
    
    crc32_header_value = binascii.crc32(clean_header)
    return crc32_header_value

def an_gpt_header(gpt_header, crc32_header_value):
    signature = gpt_header[0]
    crc32_header = gpt_header[3]
    num_of_part_entry = gpt_header[11]
    print("\t[-] Signature: %s" % signature)
    if crc32_header_value == crc32_header:
        print("\t[-] CRC32 of header: %X (VALID) => Real: %X" % (crc32_header, crc32_header_value))
    else:
        print("\t[-] WARNING!! CRC32 of header: %X (INVALID) => Real: %X" % (crc32_header, crc32_header_value))
    print("\t[-] Number of partition entries: %d" % num_of_part_entry)

def get_part_entry(fbuf, offset, size):
    ''' get partition entry '''
    return struct.unpack(GUID_PARTITION_ENTRY_FORMAT, fbuf[offset:offset+size])

def set_part_entry(part_entry):
    ''' set partition entry '''
    return struct.pack(GUID_PARTITION_ENTRY_FORMAT, part_entry[0], part_entry[1], part_entry[2], part_entry[3], part_entry[4], part_entry[5])

def get_part_table_area(f, gpt_header):
    part_entry_start_lba = gpt_header[10]
    first_use_lba_for_partitions = gpt_header[7]
    fbuf = get_lba(f, part_entry_start_lba, first_use_lba_for_partitions - part_entry_start_lba)
    
    return fbuf

def an_part_table(partition_table, gpt_header):
    ''' analysis partition table '''
    num_of_part_entry = gpt_header[11]
    size_of_part_entry = gpt_header[12]
    crc32_of_partition_array = gpt_header[13]
    
    part_list = bytearray()
    print("\n[+] Partition table")
    
    count = 0
    for part_entry_num in range(0, num_of_part_entry):
        part_entry = get_part_entry(partition_table, size_of_part_entry*part_entry_num, size_of_part_entry)
        
        # first LBA, last LBA
        if part_entry[2] == 0 or part_entry[3] == 0:
            continue
        
        count += 1
        part_list.extend(set_part_entry(part_entry))
    
    crc32_part_value = binascii.crc32(part_list)
    if crc32_part_value == crc32_of_partition_array:
        print("\t[-] CRC32 : %8X (VALID) => Real: %8X" % (crc32_of_partition_array, crc32_part_value))
    else:
        print("\t[-] WARNING!! CRC32 : %8X (INVALID) => Real: %8X" % (crc32_of_partition_array, crc32_part_value))

    print("\n[-] %d Partitions" % count)
    
    return crc32_part_value, count

def usage(argv):
    print("%s <DISK IMAGE>" % argv[0])
    sys.exit()
   
def update_gpt(disk):
    try:
        f = open(disk, "r+b")
    except IOError:
        print("[+] WARNING!! Can not open disk image.")
        sys.exit()

    fbuf = ""

    print("\n[+] Primary GPT header")
    # Primary GPT header
    gpt_header, crc32_header_value, gpt_buf = get_gpt_header(f, fbuf, PRIMARY_GPT_LBA)
    an_gpt_header(gpt_header, crc32_header_value)
    
    # Partition entries
    fbuf = get_part_table_area(f, gpt_header)
    crc32_part_value, count = an_part_table(fbuf, gpt_header)
        
    print("\n[+] Writing corrected information...")
    num_of_part_entry = count
    print("\t[-] Number of partition entries: %d" % num_of_part_entry)
    crc32_of_partition_array = crc32_part_value
    print("\t[-] CRC32 of partition array: 0x%.8X" % crc32_of_partition_array)
    gpt_header_bin = struct.pack(GPT_HEADER_FORMAT, gpt_header[0], gpt_header[1], gpt_header[2], crc32_header_value, gpt_header[4], gpt_header[5], gpt_header[6], gpt_header[7], gpt_header[8], gpt_header[9], gpt_header[10], num_of_part_entry, gpt_header[12], crc32_of_partition_array)
    crc32_header_value = calc_header_crc32(gpt_header_bin, gpt_header[2])
    print("\t[-] CRC32 of header: %X" % crc32_header_value)
    gpt_header_bin = struct.pack(GPT_HEADER_FORMAT, gpt_header[0], gpt_header[1], gpt_header[2], crc32_header_value, gpt_header[4], gpt_header[5], gpt_header[6], gpt_header[7], gpt_header[8], gpt_header[9], gpt_header[10], num_of_part_entry, gpt_header[12], crc32_of_partition_array)
    set_gpt_header(f, gpt_header_bin, PRIMARY_GPT_LBA)
        
    print("\n[+] Backup GPT header")
    ## backup GPT header
    try:
        backuplba = gpt_header[6]
        gpt_header, crc32_header_value, gpt_buf = get_gpt_header(f, fbuf, backuplba)
        an_gpt_header(gpt_header, crc32_header_value)
    
        print("[+] Writing corrected information...")
        num_of_part_entry = count
        print("\t[-] Number of partition entries: %d" % num_of_part_entry)
        crc32_of_partition_array = crc32_part_value
        print("\t[-] CRC32 of partition array: 0x%.8X" % crc32_of_partition_array)
        gpt_header_bin = struct.pack(GPT_HEADER_FORMAT, gpt_header[0], gpt_header[1], gpt_header[2], crc32_header_value, gpt_header[4], gpt_header[5], gpt_header[6], gpt_header[7], gpt_header[8], gpt_header[9], gpt_header[10], num_of_part_entry, gpt_header[12], crc32_of_partition_array)
        crc32_header_value = calc_header_crc32(gpt_header_bin, gpt_header[2])
        print("\t[-] CRC32 of header: %X" % crc32_header_value)
        gpt_header_bin = struct.pack(GPT_HEADER_FORMAT, gpt_header[0], gpt_header[1], gpt_header[2], crc32_header_value, gpt_header[4], gpt_header[5], gpt_header[6], gpt_header[7], gpt_header[8], gpt_header[9], gpt_header[10], num_of_part_entry, gpt_header[12], crc32_of_partition_array)
        set_gpt_header(f, gpt_header_bin, backuplba)
        
    except struct.error:
        print("[+] WARNING!! Backup GPT header can not found. Check your disk image.")
        print("[+] WARNING!! Backup GPT header offset: 0x%.8X" % (gpt_header[6] * LBA_SIZE))
    
    f.close()
    
if __name__ == "__main__":
    try:
        option, args = getopt.getopt(sys.argv[1:], "")

    except getopt.GetoptError as err:
        usage(sys.argv)
        sys.exit()
    
    try:
        if len(sys.argv) != 2:
            usage(sys.argv)
            sys.exit()
    
    except IndexError:
        usage()
        sys.exit()

    update_gpt(sys.argv[1])